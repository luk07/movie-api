To run project locally:

$ pip install -r requirements-dev.txt
$ cp settings/dist.env settings/.env

Fill .env file with correct values of variables

$ python manage.py runserver

Now you can request local api at 127.0.0.1:8000