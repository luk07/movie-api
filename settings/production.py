from .common import *

ALLOWED_HOSTS = ['netguru-movieapi.herokuapp.com']

SECRET_KEY = os.environ['SECRET_KEY']
OMDB_API_KEY = os.environ['OMDB_API_KEY']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ['DB_NAME'],
        'USER': os.environ['DB_USER'],
        'PASSWORD': os.environ['DB_PASSWORD'],
        'HOST': os.environ['DB_HOST'],
        'PORT': os.environ['DB_PORT'],
    }
}
