from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from tests.factories import MovieFactory
from tests.factories import MovieCommentFactory

import random


class CommentsTestCase(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.url = reverse('movies-top')
        self.start_datetime = '1900-01-01T00:00:00Z'
        self.end_datetime = '2050-01-01T00:00:00Z'

    def test_get_top_no_params(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.get(self.url, {'start_datetime': self.start_datetime})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.get(self.url, {'end_datetime': self.end_datetime})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_get_top(self):
        movies_number = random.randint(10, 20)

        movies = []
        for i in range(movies_number):
            movies.append({
                'movie': MovieFactory.create(),
                'total_comments': random.randint(1, 50)
            })
        movies.sort(key=lambda x: -x['total_comments'])

        for movie in movies:
            for i in range(movie['total_comments']):
                MovieCommentFactory.create(movie=movie['movie'])

        response = self.client.get(
            self.url,
            {
                'start_datetime': self.start_datetime,
                'end_datetime': self.end_datetime
            }
        )

        response_data = response.data
        movies_number = len(movies)
        response_length = len(response_data)

        self.assertEqual(response_length, movies_number)
        for i in range(movies_number):
            self.assertEqual(
                response_data[i]['total_comments'],
                movies[i]['total_comments']
            )
        for i in range(response_length-1):
            if response_data[i]['total_comments'] == response_data[i+1]['total_comments']:
                self.assertEqual(response_data[i]['rank'], response_data[i+1]['rank'])
