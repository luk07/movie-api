from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from tests.factories import MovieFactory
from tests.factories import MovieCommentFactory

import random


class CommentsTestCase(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.url = reverse('comment-list')
        self.movie = MovieFactory.create()

    def test_add_comments(self):
        data_set = [
            {'movie': self.movie.pk, 'text_body': 'Example text body 1'},
            {'movie': self.movie.pk, 'text_body': 'Example text body 2'},
            {'movie': self.movie.pk, 'text_body': 'Example text body 3'}
        ]

        for i in range(len(data_set)):
            response = self.client.post(self.url, data=data_set[i])
            self.assertEqual(response.status_code, 201)
            self.assertEqual(response.data['text_body'], data_set[i]['text_body'])

    def test_get_comments(self):
        comments_number = random.randint(1, 20)
        for i in range(comments_number):
            MovieCommentFactory.create(movie=self.movie)

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), comments_number)
