from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from tests.factories import MovieFactory

import random


class MoviesTestCase(APITestCase):
    def setUp(self):
        self.client = APIClient()
        self.url = reverse('movie-list')

    def test_add_movies(self):
        data_set = [
            {'title': 'BoJack Horseman'},
            {'title': 'Harry Potter and the Deathly Hallows: Part 2'},
            {'title': 'Titanic'}
        ]

        for i in range(len(data_set)):
            response = self.client.post(self.url, data=data_set[i])
            self.assertEqual(response.status_code, 201)
            self.assertEqual(response.data['title'], data_set[i]['title'])

    def test_get_movies(self):
        movies_number = random.randint(1, 20)
        for i in range(movies_number):
            MovieFactory.create()

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), movies_number)
