from movieapi.movies.models import Movie
from movieapi.movies.models import MovieComment

import factory
from factory import fuzzy
import random


class MovieFactory(factory.DjangoModelFactory):
    class Meta:
        model = Movie

    title = factory.Faker('name')


class MovieCommentFactory(factory.DjangoModelFactory):

    class Meta:
        model = MovieComment

    movie = factory.SubFactory(MovieFactory)
    text_body = fuzzy.FuzzyText(length=random.randint(5, 1000))
