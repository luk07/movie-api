from django.db import models
from django.contrib.postgres.fields import JSONField


class Movie(models.Model):

    title = models.CharField(max_length=255)
    details = JSONField(null=True)


class MovieComment(models.Model):

    movie = models.ForeignKey(Movie, on_delete=models.CASCADE, related_name='comments')
    text_body = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
