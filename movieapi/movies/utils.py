from settings import OMDB_API_URL
from settings import OMDB_API_KEY

import requests


def request_movie_details(title):
    request_params = {
        'apikey': OMDB_API_KEY,
        't': title
    }
    response = requests.get(OMDB_API_URL, request_params)
    return response.json()
