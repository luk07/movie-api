from django.db.models import Count

from rest_framework import viewsets
from rest_framework import mixins
from rest_framework import status
from rest_framework.filters import OrderingFilter
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import ParseError

from movieapi.movies.models import Movie
from movieapi.movies.models import MovieComment
from movieapi.movies.serializers import MovieSerializer
from movieapi.movies.serializers import MovieCommentSerializer
from movieapi.movies.filters import MovieFilter
from movieapi.movies.filters import MovieCommentFilter

from django_filters import rest_framework as filters


class MovieViewSet(mixins.CreateModelMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):

    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    filter_class = MovieFilter
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)
    ordering_fields = ('title',)


class MovieCommentViewSet(mixins.CreateModelMixin,
                          mixins.ListModelMixin,
                          viewsets.GenericViewSet):

    queryset = MovieComment.objects.all()
    serializer_class = MovieCommentSerializer
    filter_class = MovieCommentFilter


class TopMoviesView(APIView):

    def validate_request_params(self):
        request_params = self.request.GET
        start_datetime = request_params.get('start_datetime')
        end_datetime = request_params.get('end_datetime')
        if not start_datetime or not end_datetime:
            raise ParseError('Request requires parameters start_datetime and end_datetime.')
        return start_datetime, end_datetime

    def get_data(self):
        start_datetime, end_datetime = self.validate_request_params()
        queryset = Movie.objects.filter(
            comments__created_at__gte=start_datetime,
            comments__created_at__lte=end_datetime

        ).values('id').annotate(total_comments=Count('id')).order_by('-total_comments')

        if queryset:
            rank = 1
            query_len = len(queryset)
            for i in range(query_len - 1):
                item, next_item = queryset[i], queryset[i+1]
                item['rank'] = rank
                if item['total_comments'] != next_item['total_comments']:
                    rank += 1
            queryset[query_len - 1]['rank'] = rank

        return queryset

    def get(self, request):
        return Response(self.get_data(), status=status.HTTP_200_OK)
