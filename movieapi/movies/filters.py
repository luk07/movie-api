from django_filters import rest_framework as filters

from movieapi.movies.models import Movie
from movieapi.movies.models import MovieComment


class MovieFilter(filters.FilterSet):
    title = filters.CharFilter(field_name='title', lookup_expr='icontains')

    class Meta:
        model = Movie
        fields = ('title',)


class MovieCommentFilter(filters.FilterSet):

    class Meta:
        model = MovieComment
        fields = ('movie',)
