from rest_framework import serializers

from movieapi.movies.models import Movie
from movieapi.movies.models import MovieComment

from movieapi.movies.utils import request_movie_details


class MovieSerializer(serializers.ModelSerializer):

    class Meta:
        model = Movie
        fields = ('id', 'title', 'details')
        read_only_fields = ('details',)

    def validate(self, attrs):
        response = request_movie_details(attrs['title'])
        if response.pop('Response', response['Response']) == 'False':
            raise serializers.ValidationError(response['Error'])
        title = response.pop('Title', response['Title'])
        if Movie.objects.filter(title=title).exists():
            raise serializers.ValidationError('Movie already exists.')
        return {'title': title, 'details': response}


class MovieCommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = MovieComment
        fields = ('id', 'movie', 'text_body', 'created_at')
        read_only_fields = ('created_at',)




