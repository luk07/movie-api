"""movieapi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from django.urls import include
from django.conf.urls import url

from rest_framework.routers import SimpleRouter

from movieapi.movies.views import MovieViewSet
from movieapi.movies.views import MovieCommentViewSet
from movieapi.movies.views import TopMoviesView

api_router = SimpleRouter(trailing_slash=False)
api_router.register(r'movies', MovieViewSet, base_name='movie')
api_router.register(r'comments', MovieCommentViewSet, base_name='comment')

urlpatterns = [
    path('', include(api_router.urls)),
    url(r'top', TopMoviesView.as_view(), name='movies-top')
]
